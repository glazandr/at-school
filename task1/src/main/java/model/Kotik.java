package model;

public class Kotik {
    private int satiety = 5;
    private String name;
    private int weight;
    private int prettiness;
    private String meow;
    private static int count = 0;

    {
        count++;
    }

    public void setKotik(String name, int prettiness, int weight, String meow) {
        this.name = name;
        this.prettiness = prettiness;
        this.weight = weight;
        this.meow = meow;
    }

    public Kotik(String name, int weight, int prettiness, String meow) {
        setKotik(name, prettiness, weight, meow);
    }

    public Kotik() {
    }

    public void playWithDog() {
        if (satiety <= 0) {
            hungry();
        } else {
            satiety--;
            System.out.println("Котик поиграл со щенком. Уровень сытости " + satiety);
        }
    }

    public void sleep() {
        if (satiety <= 0) {
            hungry();
        } else {
        satiety--;
        System.out.println("Котик поспал. Уровень сытости " + satiety);
        }
    }

    public void chaseMouse() {
        if (satiety <= 0) {
            hungry();
        } else {
        satiety--;
        System.out.println("Котик поиграл с мышью. Уровень сытости " + satiety);
        }
    }

    public void playWithBall() {
        if (satiety <= 0) {
            hungry();
        } else {
        satiety--;
        System.out.println("Котик поиграл с клубком. Уровень сытости " + satiety);
        }
    }

    public void run() {
        if (satiety <= 0) {
            hungry();
        } else {
        satiety--;
        System.out.println("Котик побегал по квартире. Уровень сытости " + satiety);
        }
    }

    public void eat(int food) {
        satiety += food;
        System.out.println(("Котик вдоволь накушался. Уровень сытости " + satiety));
    }

    public void eat(int food, String kindOfFood) {
        satiety += food;
        System.out.println("Котик скушал " + kindOfFood + ". Уровень сытости " + satiety);
    }

    public void eat() {
        eat(4, "кошачий корм");
    }

    public void hungry() {
        System.out.println("Котик отказывается выполнять, т.к. хочет кушать.");
    }

    public void liveAnotherDay() {
        for (int x = 0; x < 24; x++) {
            if (satiety == 0) {
                int rand = (int) (Math.random() * 3 + 1);
                switch (rand) {
                    case 1:
                        eat(5);
                        break;
                    case 2:
                        eat(3, "немного паштета");
                        break;
                    case 3:
                        eat();
                        break;
                }
            } else {
                int rand = (int) (Math.random() * 5 + 1);
                switch (rand) {
                    case 1:
                        playWithDog();
                        break;
                    case 2:
                        sleep();
                        break;
                    case 3:
                        chaseMouse();
                        break;
                    case 4:
                        playWithBall();
                        break;
                    case 5:
                        run();
                        break;
                }
            }
        }
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getMeow() {
        return meow;
    }

    public static int getCount() {
        return count;
    }
}